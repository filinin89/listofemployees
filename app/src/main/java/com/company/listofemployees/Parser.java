package com.company.listofemployees;

import android.util.Log;

import com.company.listofemployees.model.Employee;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    public ArrayList<Employee> response;

    public ArrayList<Employee> parse(String loadedJson) {
        try {
            JSONObject jsonObject = new JSONObject(loadedJson);
            JSONArray jsonArray = jsonObject.getJSONArray("response");
            Gson gson = new Gson();
            Type type = new TypeToken<List<Employee>>() {
            }.getType();
            response = gson.fromJson(jsonObject.getJSONArray("response").toString(), type); //jsonObject.get("response");//
        } catch (JSONException e) {
            Log.d("my logs", e.getMessage());
        }
        return response;
    }
}
