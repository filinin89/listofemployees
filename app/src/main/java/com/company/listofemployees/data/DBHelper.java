package com.company.listofemployees.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.company.listofemployees.MainActivity;
import com.company.listofemployees.model.Employee;
import com.company.listofemployees.model.Specialty;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static ArrayList<Employee> employees;
    public static final String F_NAME = "f_name";
    public static final String L_NAME = "l_name";
    public static final String BIRTHDAY = "birthday";
    public static final String AVATR_URL = "avatr_url";
    public static final String SPECIALTY_ID = "specialty_id";
    public static final String SPECIALTY_NAME = "name";
    public static final String EMPLOYEE_ID = "employee_id";

    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d("logs", "--- onCreate database ---");
        employees = MainActivity.getEmployees();
        ContentValues cv = new ContentValues();

        // создаем таблицу сотрудников
        db.execSQL("create table employee" + " ("
                + "id integer primary key autoincrement,"
                + "f_name text, "
                + "l_name text, "
                + "birthday date, "
                + "avatr_url text"
                + ");");

        if (employees != null) {
            for (int i = 0; i < employees.size(); i++) {
                cv.clear();
                String f_name = employees.get(i).getF_name();
                f_name = f_name.substring(0, 1).toUpperCase() + f_name.substring(1).toLowerCase();
                cv.put(F_NAME, f_name);
                String l_name = employees.get(i).getL_name();
                l_name = l_name.substring(0, 1).toUpperCase() + l_name.substring(1).toLowerCase();
                cv.put(L_NAME, l_name);
                String birthday = employees.get(i).getBirthday();
                if(birthday == null || birthday.equals("")){
                    birthday = "-";
                }
                cv.put(BIRTHDAY, birthday);
                cv.put(AVATR_URL, employees.get(i).getAvatr_url());
                db.insert("employee", null, cv);
            }
        }

        // создаем таблицу должностей
        db.execSQL("create table specialty" + " ("
                + "specialty_id integer primary key,"
                + "name text"
                + ");");

        if (employees != null) {
            ArrayList<Specialty> selectiveSpecialties = new ArrayList<>();
            for (int i = 0; i < employees.size(); i++) {
                List<Specialty> listOfSpecialties = employees.get(i).getSpecialty();
                for (Specialty specialty : listOfSpecialties) {
                    if (!selectiveSpecialties.contains(specialty)) {
                        selectiveSpecialties.add(specialty);
                        cv.clear();
                        cv.put(SPECIALTY_ID, specialty.getSpecialty_id());
                        cv.put(SPECIALTY_NAME, specialty.getName());
                        db.insert("specialty", null, cv);
                    }
                }
            }
        }

        // создаем связующую таблицу
        db.execSQL("create table employee_specs" + "("
                + "employee_id integer, "
                + "specialty_id integer"
                + ");");

        if (employees != null) {
            for (int i = 0; i < employees.size(); i++) {
                List<Specialty> listOfSpecialties = employees.get(i).getSpecialty();
                for (Specialty specialty : listOfSpecialties) {
                    cv.clear();
                    cv.put(EMPLOYEE_ID, i + 1);
                    cv.put(SPECIALTY_ID, specialty.getSpecialty_id());
                    db.insert("employee_specs", null, cv);
                }
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "employee");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "specialty");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "employee_specs");
        onCreate(sqLiteDatabase);
    }


    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);
        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);
            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });
            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {
                alc.set(0,c);
                c.moveToFirst();
                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }


}
