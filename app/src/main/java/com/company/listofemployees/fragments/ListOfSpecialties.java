package com.company.listofemployees.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.company.listofemployees.R;
import com.company.listofemployees.data.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class ListOfSpecialties extends android.app.Fragment{

    private OnSpecialtySelectedListener mListener;

    public interface OnSpecialtySelectedListener {
        public void onSpecialtyItemSelected(Bundle bundle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSpecialtySelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnSpecialtyItemSelectedListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_speciality, null);
        DBHelper dbh = new DBHelper(getActivity());
        final SQLiteDatabase db = dbh.getWritableDatabase();

        Cursor c = db.query("specialty", null, null, null, null, null, null);
        List<String> specialtiesNames = new ArrayList<>();
        while (c.moveToNext()) {
            specialtiesNames.add(c.getString(c.getColumnIndex("name")));
        }
        c.close();

        ListView listView = (ListView) v.findViewById(R.id.lvSpeciality);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this.getActivity(), android.R.layout.simple_list_item_single_choice, specialtiesNames);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String item = (String)adapterView.getItemAtPosition(position);
                adapterView.setSelection(position);
                Cursor c1 = db.rawQuery("select specialty_id from specialty where name=?", new String[]{item});
                String specialty_id = "";
                if(c1 != null && c1.moveToFirst()){ specialty_id = c1.getString(c1.getColumnIndex("specialty_id")); c1.close(); }
                Bundle bundle = new Bundle();
                bundle.putString("specialty_id", specialty_id);
                mListener.onSpecialtyItemSelected(bundle);
            }
        });
        return v;
    }


}
