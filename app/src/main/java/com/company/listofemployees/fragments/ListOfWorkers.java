package com.company.listofemployees.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.company.listofemployees.R;
import com.company.listofemployees.data.DBHelper;

import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ListOfWorkers extends android.app.Fragment{

    private OnWorkerSelectedListener mListener;
    private static final String LOGS = "logs";

    public interface OnWorkerSelectedListener {
        public void onWorkerItemSelected(Bundle bundle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnWorkerSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnWorkerItemSelectedListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_workers, null);
        final Bundle bundle = this.getArguments();

        // Подготовим список имен - добавим только те, что были выбраны
        List<String> employeesNames = new ArrayList<>();
        String specialty_id = bundle.getString("specialty_id"); // выбранный пункт-специальность

        DBHelper dbh = new DBHelper(getActivity());
        SQLiteDatabase db = dbh.getWritableDatabase();
        Cursor c_employee_specs = db.rawQuery("select employee_id from employee_specs where specialty_id = ?", new String[]{specialty_id});
        while (c_employee_specs.moveToNext()) {
            String employee_id = c_employee_specs.getString(0);
            Cursor c_employee = db.rawQuery("select f_name, l_name, birthday from employee where id = ?", new String[]{employee_id});
            int indexF_name = c_employee.getColumnIndex("f_name");
            int indexL_name = c_employee.getColumnIndex("l_name");
            int indexBirthday = c_employee.getColumnIndex("birthday");
            while(c_employee.moveToNext()){
                String f_name = c_employee.getString(indexF_name);
                String l_name = c_employee.getString(indexL_name);
                String birthday = c_employee.getString(indexBirthday);
                String age = calculateTheAge(birthday);
                employeesNames.add(f_name + " " + l_name + " " + age);
            }
            c_employee.close();
        }
        c_employee_specs.close();

        ListView listView = (ListView) v.findViewById(R.id.lvWorkers);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this.getActivity(), android.R.layout.simple_list_item_single_choice, employeesNames);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String item = (String)adapterView.getItemAtPosition(position);
                adapterView.setSelection(position);
                String f_name = item.substring(0, item.indexOf(" "));
                bundle.putString("workerName", f_name);
                mListener.onWorkerItemSelected(bundle);
            }
        });
        return v;
    }

    public static String calculateTheAge(String birthday){
        String age = "-";
        if(!birthday.equals("-")) {
            DateTime dtBirthday = null;
            try {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
                if (birthday.substring(0, birthday.indexOf('-')).length() == 4) {
                    String[] birthdayArr = birthday.split("-");
                    String buf = birthdayArr[0];
                    birthdayArr[0] = birthdayArr[2];
                    birthdayArr[2] = buf;
                    birthday = birthdayArr[0] + "-" + birthdayArr[1] + "-" + birthdayArr[2];
                }
                dtBirthday = DateTime.parse(birthday, formatter);
            }catch (IllegalArgumentException ex){
                Log.d(LOGS, ex.getMessage());
            }
            DateTime currentDate = new DateTime();
            age = Integer.toString(Years.yearsBetween(dtBirthday, currentDate).getYears());
        }
        return age;
    }
}
