package com.company.listofemployees.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.listofemployees.R;
import com.company.listofemployees.data.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class ListOfDetails extends android.app.Fragment {

    private TextView tvF_name;
    private TextView tvL_name;
    private TextView tvBirthday;
    private TextView tvAge;
    private TextView tvSpecialty;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details, null);
        tvF_name = (TextView) v.findViewById(R.id.tvF_name);
        tvL_name = (TextView) v.findViewById(R.id.tvL_name);
        tvBirthday = (TextView) v.findViewById(R.id.tvBirthday);
        tvAge = (TextView) v.findViewById(R.id.tvAge);
        tvSpecialty = (TextView) v.findViewById(R.id.tvSpecialty);

        Bundle bundle = this.getArguments();
        String workerName = bundle.getString("workerName"); // выбранный пункт-человек
        String specialty_id = bundle.getString("specialty_id");

        DBHelper dbh = new DBHelper(getActivity());
        SQLiteDatabase db = dbh.getWritableDatabase();

        Cursor c_specialty = db.rawQuery("select name from specialty where specialty_id =?", new String[]{specialty_id}); // получим специальность
        String specialty_name = "";
        if(c_specialty != null && c_specialty.moveToFirst()){ specialty_name = c_specialty.getString(c_specialty.getColumnIndex("name")); c_specialty.close(); }

        Cursor c_employee = db.rawQuery("select l_name, birthday from employee where f_name = ?", new String[]{workerName});
        String f_name = workerName;
        String l_name="";
        String birthday="";
        if(c_employee != null && c_employee.moveToFirst()){
            l_name = c_employee.getString(c_employee.getColumnIndex("l_name"));
            birthday = c_employee.getString(c_employee.getColumnIndex("birthday"));
            c_employee.close(); }
        String age = ListOfWorkers.calculateTheAge(birthday);

        tvF_name.setText(getString(R.string.f_name, f_name));
        tvL_name.setText(getString(R.string.l_name, l_name));
        tvBirthday.setText(getString(R.string.birthday, birthday));
        tvAge.setText(getString(R.string.age, age));
        tvSpecialty.setText(getString(R.string.specialty, specialty_name));

        return v;
    }

}
