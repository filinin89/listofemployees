package com.company.listofemployees;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.company.listofemployees.data.DBHelper;
import com.company.listofemployees.fragments.ListOfDetails;
import com.company.listofemployees.fragments.ListOfSpecialties;
import com.company.listofemployees.fragments.ListOfWorkers;
import com.company.listofemployees.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements ListOfSpecialties.OnSpecialtySelectedListener, ListOfWorkers.OnWorkerSelectedListener /*implements View.OnClickListener*/{

    private FragmentTransaction fTrans;
    private Parser parser;
    private static ArrayList<Employee> employees;
    private final String LOG_TAG = "myLogs";
    private Button buttonDatabase;


    public static ArrayList<Employee> getEmployees() {
        return employees;
    }

    public static void setEmployees(ArrayList<Employee> employees) {
        MainActivity.employees = employees;
    }

    public FragmentTransaction getfTrans() {
        return fTrans;
    }

    public void setfTrans(FragmentTransaction fTrans) {
        this.fTrans = fTrans;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonDatabase = (Button) findViewById(R.id.buttonDatabaseActvt);
        buttonDatabase.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            Intent dbmanager = new Intent(MainActivity.this,AndroidDatabaseManager.class);
            startActivity(dbmanager);
        }
    });

        // Download json
        Loader loader = new Loader();
        loader.execute();
        String loadedJson = "";
        try {
            loadedJson = loader.get();
        } catch (InterruptedException e) {
            Log.d(LOG_TAG, e.getMessage());
        } catch (ExecutionException e) {
            Log.d(LOG_TAG, e.getMessage());
        }

        // Parse
        parser = new Parser();
        employees = parser.parse(loadedJson);

        // Wrte in DB
        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();

        // View
        fTrans = getFragmentManager().beginTransaction();
        ListOfSpecialties frag1 = new ListOfSpecialties();
        fTrans.add(R.id.fragmentController, frag1);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSpecialtyItemSelected(Bundle bundle) {
        fTrans = getFragmentManager().beginTransaction();
        ListOfWorkers frag2 = new ListOfWorkers();
        frag2.setArguments(bundle);
        fTrans.replace(R.id.fragmentController, frag2);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }

    @Override
    public void onWorkerItemSelected(Bundle bundle) {
        fTrans = getFragmentManager().beginTransaction();
        ListOfDetails frag3 = new ListOfDetails();
        frag3.setArguments(bundle);
        fTrans.replace(R.id.fragmentController, frag3);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }

}
