package com.company.listofemployees;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;



public class Loader extends AsyncTask<Void, Void, String> {
    @Override
    protected String doInBackground(Void... voids) {

        String loadedJson = "";
        try {
            URL url = new URL("http://gitlab.65apps.com/65gb/static/raw/master/testTask.json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            loadedJson = IOUtils.toString(connection.getInputStream());
        } catch (MalformedURLException e) {
            Log.d("my logs", e.getMessage());
        } catch (IOException e) {
            Log.d("my logs", e.getMessage());
        }
        return loadedJson;

    }
}
