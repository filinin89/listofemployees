package com.company.listofemployees.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Employee {

    @SerializedName("f_name")
    private String f_name;

    @SerializedName("l_name")
    private String l_name;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("avatr_url")
    private String avatr_url;

    @SerializedName("specialty")
    private List<Specialty> specialty;

    public Employee(String f_name, String l_name, String birthday, String avatr_url, List<Specialty> specialty) {
        this.f_name = f_name;
        this.l_name = l_name;
        this.birthday = birthday;
        this.avatr_url = avatr_url;
        this.specialty = specialty;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatr_url() {
        return avatr_url;
    }

    public void setAvatr_url(String avatr_url) {
        this.avatr_url = avatr_url;
    }

    public List<Specialty> getSpecialty() {
        return specialty;
    }

    public void setSpecialty(List<Specialty> specialty) {
        this.specialty = specialty;
    }
}
