package com.company.listofemployees.model;

import com.google.gson.annotations.SerializedName;

public class Specialty {

    @SerializedName("specialty_id")
    private int specialty_id;

    @SerializedName("name")
    private String name;

    public Specialty(int specialty_id, String name) {
        this.specialty_id = specialty_id;
        this.name = name;
    }

    public int getSpecialty_id() {
        return specialty_id;
    }

    public void setSpecialty_id(int specialty_id) {
        this.specialty_id = specialty_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        Specialty specialty = (Specialty)obj;
        if(this.getName().equals(specialty.getName())) return true;
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        return "Specialty{" +
                "name='" + name + '\'' +
                '}';
    }
}
