package com.company.listofemployees.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EmployeeApi {

    @GET("testTask.json")
    Call<List<Employee>> getEmployees();
}
